package com.zea

fun fibonacciOf(n: Int): Int {
    if(n == 0) return 0
    if(n == 1) return 1
    return fibonacciOf(n - 1) + fibonacciOf(n - 2)
}
