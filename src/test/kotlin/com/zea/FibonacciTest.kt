package com.zea

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.TestFactory

class FibonacciTest {

    @TestFactory()
    fun fibonacciTest() = listOf(
        0 to 0,
        1 to 1,
        2 to 1,
        3 to 2,
        4 to 3,
        5 to 5,
        6 to 8
    )
        .map { (input, expected) -> DynamicTest.dynamicTest(
            "Fibonacci of (${input - 1}) is: $expected"
        )
        { assertEquals(expected, fibonacciOf(input)) }
        }
}
